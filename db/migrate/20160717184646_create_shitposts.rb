class CreateShitposts < ActiveRecord::Migration
  def change
    create_table :shitposts do |t|
      t.string :title
      t.text :content

      t.timestamps null: false
    end
  end
end
