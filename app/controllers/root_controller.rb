class RootController < ApplicationController
  def index
    if session[:authorized] then
      @posts = Shitpost.all
    else
      redirect_to controller: 'auth', action: 'auth'
    end
  end
end
