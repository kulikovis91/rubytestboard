class ShitpostController < ApplicationController
  def add
    if session[:authorized] then
      post = Shitpost.new
      post.title = params[:title]
      post.content = params[:content]
      post.save
      redirect_to controller: 'root', action: 'index'
    else 
      redirect_to controller: 'auth', action: 'auth'
    end
  end
end
